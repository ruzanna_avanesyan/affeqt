$(function () {
    var a = {
        xScale: "ordinal",
        yScale: "linear",
        main: [{className: ".pizza", data: [{x: "1.2", y: 4}, {x: "1.3", y: 8},{x: "1.4", y: 4}, {x: "1.6", y: 8}]}]
    };
    var b = {
        xScale: "ordinal",
        yScale: "linear",
        main: [{className: ".pizza", data: [{x: "Test Avg Diffuculty", y: 4}]}]
    };
    var c = {
        xScale: "ordinal",
        yScale: "linear",
        main: [{className: ".pizza", data: [{x: "1.2", y: 4}, {x: "1.3", y: 8}]}]
    };
    var d = {
        xScale: "ordinal",
        yScale: "linear",
        main: [{className: ".pizza", data: [{x: "1.0", y: 2}, {x: "2.0", y: 8}]}]
    };
    new xChart("bar", a, "#example1");
    new xChart("bar", b, "#example2");
    new xChart("bar", c, "#example3");
    new xChart("bar", d, "#example4");
}), $(function () {
    var a = document.createElement("div"),
        b = -(~~$("html").css("padding-left").replace("px", "") + ~~$("body").css("margin-left").replace("px", "")),
        c = -32;
    a.className = "ex-tooltip", document.body.appendChild(a);
    var d = {
        xScale: "time",
        yScale: "linear",
        main: [{
            className: ".pizza",
            data: [{x: "2012-11-05", y: 6}, {x: "2012-11-06", y: 6}, {x: "2012-11-07", y: 8}, {
                x: "2012-11-08",
                y: 3
            }, {x: "2012-11-09", y: 4}, {x: "2012-11-10", y: 9}, {x: "2012-11-11", y: 6}]
        }]
    }, e = {
        dataFormatX: function (a) {
            return d3.time.format("%Y-%m-%d").parse(a)
        }, tickFormatX: function (a) {
            return d3.time.format("%A")(a)
        }, mouseover: function (d, e) {
            var f = $(this).offset();
            $(a).text(d3.time.format("%A")(d.x) + ": " + d.y).css({top: c + f.top, left: f.left + b}).show()
        }, mouseout: function (b) {
            $(a).hide()
        }
    };
    // new xChart("line-dotted", d, "#example4", e)
});